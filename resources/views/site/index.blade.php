@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-lg-6 offset-3" style="margin-top: 100px;">
            <form action="#" method="post">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label for="">Phone:</label>
                    <input type="text" name="client_phone" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Name:</label>
                    <input type="text" name="client_name" class="form-control">
                </div>
                <div class="form-group">
                    <input type="submit" name="client_send" class="btn btn-dark">
                </div>
            </form>
        </div>
    </div>
@endsection
