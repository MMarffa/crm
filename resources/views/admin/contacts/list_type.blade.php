@extends('layouts.main')
<style media="screen">
    .inputReadItem{
        background: transparent;
        border: none;
        outline: none;
        color: white;
    }
</style>
@section('content')
<div class="row">
    <div class="col-lg-12">
        <button type="button" class="createItem btn btn-success">Додати</button>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-hover">
            <thead>
                <th>ID</th>
                <th>TYPE</th>
                <th></th>
            </thead>
            <tbody>
                @foreach($data['contacts'] as $contact)
                    <tr item_id="{{$contact->contact_id}}">
                        <td>{{$contact->contact_id}}</td>
                        <td><input item_id="{{$contact->contact_id}}" value="{{$contact->contact_name}}" readonly class="inputReadItem"></td>
                        <td>
                            <span item_id="{{$contact->contact_id}}" class="editItem"><i class="material-icons text-success">edit</i></span>
                            <span item_id="{{$contact->contact_id}}" class="saveItem" style="display:none;"><i class="material-icons text-success">done</i></span>
                            <span item_id="{{$contact->contact_id}}" class="removeItem"><i class="material-icons text-info">close</i></span>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

<script src="https://code.jquery.com/jquery-3.4.1.min.js">  </script>
<script>
    $(document).ready(function(){

        var TEMPLATE = {
            rowTemplate: '<tr item_id="[item_id]">' +
                                '<td>[item_id]</td>' +
                                '<td><span class="bmd-form-group is-filled"><input item_id="[item_id]" value="[item_value]" class=""></span></td>' +
                                '<td>' +
                                    '<span item_id="[item_id]" class="editItem" style="display: none;"><i class="material-icons text-success">edit</i></span>' +
                                    '<span item_id="[item_id]" class="saveItem" style=""><i class="material-icons text-success">done</i></span>' +
                                    '<span item_id="[item_id]" class="removeItem"><i class="material-icons text-info">close</i></span>' +
                                '</td>' +
                            '</tr>',
        };

        /*
        * EDIT ITEM start
        */
        $(document).on('click', '.editItem', function(){
            let item_id = $(this).attr('item_id');
            $(this).hide();
            $('.saveItem[item_id="'+item_id+'"]').show();
            $('input[item_id="'+item_id+'"]').removeAttr('readonly');
            $('input[item_id="'+item_id+'"]').removeClass('inputReadItem');
            $('input[item_id="'+item_id+'"]').focus();
        });

        $(document).on('click', '.saveItem', function(){
            let item_id = $(this).attr('item_id');
            let value = '';
            if(!item_id){
                item_id = 'new';
                value = $('input[item_id=""]').val();
            }else{
                value = $('input[item_id="'+item_id+'"]').val();
            }
            var _this = this;
            $.ajax({
                url: '/admin/contacts/api/save_contact_type',
                data: {
                    item_id: item_id,
                    value: value,
                },
                type: 'POST',
                success(response){
                    response = JSON.parse(response);
                    if(item_id !== 'new'){
                        $(_this).hide();
                    }else{
                        item_id = response.contact_id;
                        $('tr[item_id=""]').remove();
                        let itemTemplate = TEMPLATE.rowTemplate;
                        itemTemplate = itemTemplate.replace(/\[item_id\]/gi, response.contact_id).replace(/\[item_value\]/gi, response.contact_name);
                        $('table tbody').append(itemTemplate);
                    }

                    $('.editItem[item_id="'+item_id+'"]').show();
                    $('.saveItem[item_id="'+item_id+'"]').hide();
                    $('input[item_id="'+item_id+'"]').attr('readonly', 'readonly');
                    $('input[item_id="'+item_id+'"]').addClass('inputReadItem');
                    $('input[item_id="'+item_id+'"]').val(response.contact_name);
                }
            });
        });
        /*
        * EDIT ITEM end
        */
        /*
        * REMOVE ITEM start
        */
        $(document).on('click', '.removeItem', function(){
            var canRemove = confirm("Ви впевнені що бажаєте видалити?");
            if(!canRemove) return;
            let item_id = $(this).attr('item_id');
            if(item_id.trim()===''){
                $('tr[item_id=""]').remove();
                return;
            }
            $.ajax({
                url: '/admin/contacts/api/remove_contact_type',
                data: {
                    item_id: item_id,
                },
                type: 'POST',
                success(response){
                    response = JSON.parse(response);
                    if(typeof response !== 'undefined'){
                        response = parseInt(response);
                        if(!isNaN(response) && response === 1){
                            $('tr[item_id="'+item_id+'"]').remove();
                        }
                    }
                }
            });
        });
        /*
        * REMOVE ITEM end
        */
        /*
        * CREATE ITEM start
        */
        $(document).on('click', '.createItem', function(){
            if($('tr[item_id=""]').length) return;
            let itemTemplate = TEMPLATE.rowTemplate;
            itemTemplate = itemTemplate.replace(/\[item_id\]/gi, '').replace(/\[item_value\]/gi, '');
            $('table tbody').prepend(itemTemplate);
        });
        /*
        * CREATE ITEM end
        */

    });
</script>
