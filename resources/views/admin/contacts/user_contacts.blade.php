<table class="table table-hover">
    <tbody>
        @foreach($data['contacts'] as $contact)
            <tr>
                <td>{{$contact->user_id}}</td>
                <td>{{$contact->type_id}}</td>
                <td>{{$contact->contact_value}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
