@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card card-profile">
                <div class="card-avatar">
                    <a href="/admin/users/edit/{{$data['user']->user_id}}"><img class="img" src="/template/img/faces/marc.jpg"></a>
                </div>
                <div class="card-body">
                    <h6 class="card-category">{{$data['user']->user_name}}</h6>
                    <h4 class="card-title">{{$data['user']->user_email}}</h4>
                    <p class="card-description">Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...</p>
                    <a href="/admin/users/edit/{{$data['user']->user_id}}" class="btn btn-primary btn-round">EDIT</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="">Тип контакту</label>
                        <select class="form-control" name="type">
                            @foreach($data['types'] as $type)
                                <option value="{{$type->contact_id}}">{{$type->contact_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Контакт</label>
                        <input type="text" class="form-control" name="conact">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="createContact btn btn-primary" value="Додати">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body contactsBody">

                </div>
            </div>
        </div>
    </div>
@endsection
<script src="https://code.jquery.com/jquery-3.4.1.min.js">  </script>
<script>
    $(document).ready(function(){

        $.ajax({
            url: '/admin/contacts/api/contact_table_by_userid/2',
            type: 'GET',
            success(response){
                $('.contactsBody').html(response);
            },
        });

        $(document).on('click', '.createContact', function(){
            let contact_type = $('select[name="type"]').val();
            let contact = $('input[name="conact"]').val();
            let user = 2;
            $.ajax({
                url: '/admin/contacts/api/create_contact',
                data: {
                    contact_type: contact_type,
                    contact: contact,
                    user: user,
                },
                type: 'POST',
                success(response){
                    response = JSON.parse(response);
                    $.ajax({
                        url: '/admin/contacts/api/contact_table_by_userid/2',
                        type: 'GET',
                        success(response){
                            $('.contactsBody').html(response);
                        },
                    });
                }
            });
        });
    });
</script>
