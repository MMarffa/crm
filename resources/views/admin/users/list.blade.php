@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <table class="table table-hover">
            <thead>
                <th>ID</th>
                <th>Name</th>
                <th>EMAIL</th>
                <th>PHONE</th>
                <th>REGISTER</th>
                <th></th>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->user_id}}</td>
                        <td>{{$user->user_name}}</td>
                        <td>{{$user->user_email}}</td>
                        <td>{{$user->user_phone}}</td>
                        <td>{{date('d.m.Y',strtotime($user->user_register))}}</td>
                        <td>
                            <a href="/admin/users/edit/{{$user->user_id}}"><i class="material-icons text-success">edit</i></a>
                            <a href="/admin/users/view/{{$user->user_id}}"><i class="material-icons text-info">person</i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
