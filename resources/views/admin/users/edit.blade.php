@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card card-profile">
                <div class="card-avatar">
                    <a href="#"><img class="img" src="/template/img/faces/marc.jpg"></a>
                </div>
                <div class="card-body">
                    <h6 class="card-category">{{$data['user']->user_name}}</h6>
                    <h4 class="card-title">{{$data['user']->user_email}}</h4>
                    <p class="card-description">Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...</p>
                </div>
            </div>
        </div>
        <div class="col-md-8">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Edit Profile</h4>
              <p class="card-category">Complete your profile</p>
            </div>
            <div class="card-body">
              <form action="#" method="post">
                  {!! csrf_field() !!}
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                          <label class="bmd-label-floating">User name</label>
                          <input type="text" name="user_name" value="{{$data['user']->user_name}}" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                          <label class="bmd-label-floating">Email address</label>
                          <input type="email" name="user_email" value="{{$data['user']->user_email}}" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                          <label class="bmd-label-floating">User phone</label>
                          <input type="number" name="user_phone" value="{{$data['user']->user_phone}}" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group bmd-form-group">
                          <label class="bmd-label-floating">User Password</label>
                          <input autocomplete="off" type="password" name="user_password" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group bmd-form-group">
                          <label class="bmd-label-floating">Password confirm</label>
                          <input autocomplete="off" type="password" name="user_confirm" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2">
                        <div class="form-group">
                            <label class="bmd-label-floating">SEX</label>
                        </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group">
                            <label>M <input name="user_sex" {{($data['user']->user_sex == 0 ? 'checked' : '' )}} value="0" type="radio" class="form-control"></label>
                            <label>Ж <input name="user_sex" {{($data['user']->user_sex == 1 ? 'checked' : '' )}} value="1" type="radio" class="form-control"></label>
                          </div>
                      </div>
                    </div>

                    <div class="">
                        @foreach($data['alerts'] as $alert)
                            <div class="alert alert-{{$alert['type']}}">{{$alert['message']}}</div>
                        @endforeach
                    </div>

                    <button type="submit" name="save_profile" class="btn btn-primary pull-right">Update Profile</button>
                    <div class="clearfix"></div>
              </form>
            </div>
          </div>
        </div>
    </div>
@endsection
