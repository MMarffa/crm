<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*
* ADMIN
* /admin/*
*/
Route::get('admin/users/list', 'admin\\users\\UsersController@list'); // Список користувачів
Route::get('admin/users/view/{user_id}', 'admin\\users\\UsersController@view'); // Список користувачів
Route::get('admin/users/edit/{user_id}', 'admin\\users\\UsersController@edit'); // Список користувачів
Route::post('admin/users/edit/{user_id}', 'admin\\users\\UsersController@edit'); // Список користувачів

Route::get('admin/contacts/list_type/', 'admin\\contacts\\ContactController@list_type'); // Список користувачів
Route::post('admin/contacts/api/save_contact_type', 'admin\\contacts\\ApiController@save_contact_type'); // Список користувачів
Route::post('admin/contacts/api/remove_contact_type', 'admin\\contacts\\ApiController@remove_contact_type'); // Список користувачів
Route::post('admin/contacts/api/create_contact', 'admin\\contacts\\ApiController@create_contact'); // Список користувачів
Route::post('admin/contacts/api/contact_table_by_userid', 'admin\\contacts\\ApiController@contact_table_by_userid'); // Список користувачів
Route::get('admin/contacts/api/contact_table_by_userid/{user_id}', 'admin\\contacts\\ApiController@contact_table_by_userid'); // Список користувачів
// Route::get('admin/contacts/api/save_contact_type/', 'admin\\contacts\\ApiController@save_contact_type'); // Список користувачів


/*
* SiteController start
*/
// Route::get('photos/{photo_id}', 'SiteController@photos');
// Route::get('phantom', 'SiteController@phantom');
// Route::get('index', 'SiteController@index');
// Route::post('index', 'SiteController@index');
/*
* SiteController end
*/
