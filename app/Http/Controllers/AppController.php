<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class AppController extends BaseController{

    public $alerts = []; // TODO

    public function setAlert($type = false, $message = false){
        if($type && $message){
            $this->alerts[] = [
                'type' => $type,
                'message' => $message,
            ];
        }
    }

    public function setUserLog($className = false, $methodName = false){
        if($className && $methodName){
            DB::table('users_log')
                ->insert([
                    'log_class' => $className,
                    'log_method' => $methodName,
                ]);
        }
    }

}
