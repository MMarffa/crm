<?php

namespace App\Http\Controllers\admin\contacts;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\AppController as BaseController;

class ContactController extends BaseController{

    public function list_type(){
        $this->setUserLog(__CLASS__, __FUNCTION__);
        $data = [];
        $data['contacts'] = DB::table('contacts_type')
                                ->where('visible','1')
                                ->get();
        return view('admin.contacts.list_type', compact('data'));
    }

}
