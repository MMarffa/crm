<?php

namespace App\Http\Controllers\admin\contacts;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\AppController as BaseController;

class ApiController extends BaseController{

    public $result = [];

    public function __destruct(){
        if($this->result) print_r(json_encode($this->result));
    }

    public function save_contact_type(Request $request){
        $this->setUserLog(__CLASS__, __FUNCTION__);
        if($request->item_id !== 'new'){
            DB::table('contacts_type')
                ->where('contact_id', $request->item_id)
                ->update(['contact_name' => $request->value]);
            $this->result = DB::table('contacts_type')
                ->where('contact_id', $request->item_id)
                ->first();
        }else{
            $contact_id = DB::table('contacts_type')
                ->insertGetId([
                    'contact_name' => $request->value
                ]);
            $this->result = DB::table('contacts_type')
                ->where('contact_id', $contact_id)
                ->first();
        }
    }

    public function remove_contact_type(Request $request){
        $this->setUserLog(__CLASS__, __FUNCTION__);
        $this->result = DB::table('contacts_type')
            ->where('contact_id', $request->item_id)
            ->update(['visible' => '0']);
        // $result = DB::table('contacts_type')
        //     ->where('contact_id', $request->item_id)
        //     ->delete();
    }

    public function create_contact(Request $request){
        $this->setUserLog(__CLASS__, __FUNCTION__);
        $contact_type = $request->contact_type;
        $contact = $request->contact;
        $user = $request->user;
        $contact_id = DB::table('contacts')
            ->insertGetId([
                'user_id' => $user,
                'type_id' => $contact_type,
                'contact_value' => $contact,
            ]);
        $this->result = DB::table('contacts')
            ->where('contact_id', $contact_id)
            ->first();
    }

    // public function contact_table_by_userid(Request $request){
    public function contact_table_by_userid($user_id){
        $this->result = false;
        $data = [];
        $data['contacts'] = DB::table('contacts')
                                ->where('user_id', $user_id)
                                ->get();
        return view('admin.contacts.user_contacts', compact('data'));
    }

}
