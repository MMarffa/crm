<?php

namespace App\Http\Controllers\admin\users;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\AppController as BaseController;

class UsersController extends BaseController{

    // Список користувачів
    public function list(){
        $this->setUserLog(__CLASS__, __FUNCTION__);
        // $start = microtime(true);
        // echo 'Время выполнения скрипта: '.round(microtime(true) - $start, 4).' сек.';
        $users = DB::table('users')->get()->toArray();
        return view('admin.users.list', compact('users'));
    }

    public function view($user_id = false){
        $data = [];
        $data['user'] = [];
        if($user_id){
            $this->setUserLog(__CLASS__, __FUNCTION__);
            $data['user'] = $this->getUserById($user_id);
        }
        $data['types'] = DB::table('contacts_type')
                            ->where('visible', '1')
                            ->get();
        return view('admin.users.view', compact('data'));
    }

    public function edit($user_id, Request $request){
        $this->setUserLog(__CLASS__, __FUNCTION__);
        $data = [];
        if ($request->isMethod('post')) {
            $correctData = true;
            $user = $this->getUserById($user_id);
            $update_data = [];
            $update_data['sex'] = $request->user_sex;
            $update_data['name'] = $request->user_name;
            $update_data['email'] = $request->user_email;
            $update_data['phone'] = $request->user_phone;
            $update_data['password'] = $request->user_password == $request->user_confirm ? $request->user_password : false;
            $update_data['password'] = $update_data['password'] ? md5($update_data['password']) : $user->user_password;
            // TODO ГЕНЕРАЦІЯ ТА ВИВЕДЕННЯ ПОМИЛОК
            if($correctData){
                DB::table('users')
                    ->where('user_id', $user_id)
                    ->update([
                        'user_name' => $update_data['name'],
                        'user_email' => $update_data['email'],
                        'user_password' => $update_data['password'],
                        'user_phone' => $update_data['phone'],
                        'user_sex' => $update_data['sex'],
                    ]);
            }
            // $this->setAlert('danger', 'Test');
            // $this->setAlert('success', 'Test');
        }
        $data['user'] = $this->getUserById($user_id);
        $data['alerts'] = $this->alerts;
        return view('admin.users.edit', compact('data'));
    }

    public function getUserById($userId){
        $this->setUserLog(__CLASS__, __FUNCTION__);
        return DB::table('users')
                    ->where('user_id', $userId)
                    ->first();
    }

}
