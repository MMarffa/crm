<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Telegram\Bot\Api;
use JonnyW\PhantomJs\Client;

class SiteController extends BaseController{

    public $token = '889711597:AAHS5vjR8cqHFLv7hCuYR9Jh2hIlI9lzytI';
    public $telegram = false;

    public function __construct(){
        $this->telegram = new Api($this->token);
    }

    public function index(Request $request){
        $data = [];
        if ($request->isMethod('post')) {
            $id = DB::table('clients')
                ->insertGetId([
                    'phone' => $request->client_phone,
                    'name' => $request->client_name,
                ]);
            if($id){
                $message = "Клієнт №".$id."\n";
                $message .= "Phone №".$request->client_phone."\n";
                $message .= "Name №".$request->client_name."\n";
                $this->telegram->sendMessage([
                  'chat_id' => 591679918,
                  'text' => $message
                ]);
            }
        }
        return view('site.index', compact('data'));
    }

    public function photos($photo_id = false){
        $photo = DB::table('user_photos')
            ->where('photo_id', $photo_id)
            ->first();

        dd($photo);
    }

    public function phantom(){
        $client = Client::getInstance();
        $request = $client->getMessageFactory()->createRequest("http://kino.i.ua/film/1/", 'GET');
        $response = $client->getMessageFactory()->createResponse();
        $request->addHeader('User-Agent', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36');
        $request->addHeader('Accept-Language', 'uk,uk;q=0.8,en-US;q=0.5,en;q=0.3');
        $client->send($request, $response);
        $html = $response->getContent();
    }




}
