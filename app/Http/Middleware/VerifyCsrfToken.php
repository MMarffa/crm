<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'admin/contacts/api/save_contact_type',
        'admin/contacts/api/remove_contact_type',
        'admin/contacts/api/create_contact',
    ];
}
